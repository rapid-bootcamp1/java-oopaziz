public class Person {

    //List Field
    public Integer id;
    public String name;
    public String gender;
    public String address;
    public int age;

    public Person() {
    }

    public Person(Integer id, String name, String gender, String address, int age) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.address = address;
        this.age = age;
    }

    //List Of Method Getter Setter
    public void sayHello(){
        System.out.println("My Name" +name+ "i working at" +address);
    }

}
