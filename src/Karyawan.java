public class Karyawan extends  Person {
    private Integer IdKaryawan;
    private String Jabatan;
    private Integer LamaBekerja;

    public Karyawan(Integer id, String name, String gender, String address, int age, Integer idKaryawan, String jabatan, Integer lamaBekerja) {
        super(id, name, gender, address, age);
        IdKaryawan = idKaryawan;
        Jabatan = jabatan;
        LamaBekerja = lamaBekerja;
    }
}
