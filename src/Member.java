public class Member extends Person{
    private Integer idMember;
    private String memberSince;
    private String memberName;

    public Member(Integer id, String name, String gender, String address, int age, Integer idMember, String memberSince, String memberName) {
        super(id, name, gender, address, age);
        this.idMember = idMember;
        this.memberSince = memberSince;
        this.memberName = memberName;
    }
}
