package Polymorphism;

public class MainPolymorp {
    public static void main(String[] args) {
        Employee employee = new Employee("Aziz");
        Manager manager = new Manager("Majid");
        VicePresident president = new VicePresident("Majidul Aziz");

//        employee.sayHello(); manager.sayHello(); president.sayHello();
        sayHello(president);
        sayHello(manager);
        sayHello(employee);


    }
    public static void sayHello(Employee employee){
        if(employee instanceof VicePresident){
            VicePresident vicePresident = (VicePresident) employee;
            System.out.println("Hello VP "+ vicePresident.name);
        } else if (employee instanceof Manager) {
            Manager manager = (Manager) employee;
            System.out.println("Hello Manager "+ manager.name);
        }else {
            System.out.println("Hello Employee "+ employee.name);
        }
    }

}
