public class MainPerson {
    public static void main(String[] args) {
        //class - object - instance - constructor
        Person person = new Person();
        person.id = 1;
        person.name = "Aziz";
        person.address ="Tapanuli";
        person.gender = "Laki-Laki";
        person.age =20;
        person.sayHello();
    }
}