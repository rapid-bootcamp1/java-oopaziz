package Encapsulasi;

public class Main {
    public static void main(String[] args) {
        Category category = new Category();
        category.setName("Rapid TechNologi");
        category.setDec("IT Solution");
        category.setId(1);

        System.out.println(category);
        System.out.println(new Category(2, "Rapid Technologi", "IT Solution Departement"));
        System.out.println(new Category(3, "Astra Graphia Information Technologi", "Agit"));
    }
}
