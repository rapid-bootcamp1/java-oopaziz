package Encapsulasi;

import java.util.StringJoiner;

public class Category {
    private int Id;
    private String name;
    private String dec;

    public Category() {
    }

    public Category(int id, String name, String dec) {
        Id = id;
        this.name = name;
        this.dec = dec;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDec() {
        return dec;
    }

    public void setDec(String dec) {
        this.dec = dec;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Category.class.getSimpleName() + "[", "]")
                .add("Id=" + Id)
                .add("name='" + name + "'")
                .add("dec='" + dec + "'")
                .toString();
    }
}
