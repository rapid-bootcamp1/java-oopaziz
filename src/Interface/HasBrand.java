package Interface;

public interface HasBrand {
    String getBrand();
    Integer getPrice();
}
