package Interface;

public class Main {
    public static void main(String[] args) {
        HasBrand hasBrand = new HasBrandImpl("Clube Air Mineral", 2000);
        System.out.println("Brand: " + hasBrand.getBrand() +  "\n"+"Price: " + hasBrand.getPrice());
    }
}
