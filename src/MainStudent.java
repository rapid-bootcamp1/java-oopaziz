public class MainStudent {
    public static void main(String[] args) {
        Student student = new Student(
                1,
                "Aziz",
                "Laki-Laki",
                "Bekasi",
                20,"Teknik Informatika", 1
        );
        System.out.println(student);

        //Karyawan
        Karyawan karyawan = new Karyawan(
                1, "Aziz", "Laki-Laki", "Bekasi", 25,
                2, "Karyawan Biasa", 2
        );
        System.out.println(karyawan);

        //Teacher
        Teacher teacher = new Teacher(
                2, "Majidul", "Laki-Laki", "Tapanuli", 21,
                12, "Matematika", "Rapid Academy"
        );

        System.out.println(teacher);

        //Member
        Member member = new Member(
                3, "Majid", "Laki-Laki", "Jawa Barat", 19,
                123, "2022", "VIP"
        );
        System.out.println(member);

        //Group
        Group group = new Group(
                123, "Aziz Majidul", "Laki-Laki", "Padang Lawas", 23,
                123, "Group A"
        );

        System.out.println(group);
    }
}
