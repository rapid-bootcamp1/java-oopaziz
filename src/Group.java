public class Group extends  Person{
    private Integer groupId;
    private String groupName;

    public Group(Integer id, String name, String gender, String address, int age, Integer groupId, String groupName) {
        super(id, name, gender, address, age);
        this.groupId = groupId;
        this.groupName = groupName;
    }
}
