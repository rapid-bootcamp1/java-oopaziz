import Logic.Logic2.Soal1;
import Logic.Logic1.*;
import Logic.Logic2.Soal2_2;

public class MainLogic {
    public static void main(String[] args) {
        System.out.println("Logic1 Soal 1: ");
        Logic1Soal1 logic1Soal1 = new Logic1Soal1(9);
        logic1Soal1.cetakArray();

        System.out.println("Logic1 Soal 2: ");
        Soal2 soal2 = new Soal2(9);
        soal2.cetakArray();

        System.out.println("Logic1 Soal 3: ");
        Soal3 soal3 = new Soal3(9);
        soal3.cetakArray();

        System.out.println("Logic1 Soal 4: ");
        Soal4 soal4 = new Soal4(9);
        soal4.cetakArray();

        System.out.println("Logic1 Soal 5: ");
        Soal5 soal5  = new Soal5(9);
        soal5.cetakArray();

        System.out.println("Logic1 Soal 6: ");
        Soal6 soal6 = new Soal6(9);
        soal6.cetakArray();

        System.out.println("Logic1 Soal 7: ");
        Soal7 soal7 = new Soal7(9);
        soal7.cetakArray();

        System.out.println("Logic1 Soal 8: ");
        Soal8 soal8 = new Soal8(9);
        soal8.cetakArray();

        System.out.println("Logic1 Soal 9: ");
        Soal9 soal9 = new Soal9(9);
        soal9.cetakArray();

        System.out.println("Logic1 Soal 10: ");
        Soal10 soal10 = new Soal10(9);
        soal10.cetakArray();


        System.out.println("Logic2 Soal 1: ");
        Soal1 soal1 = new Soal1(9);
        soal1.cetakArray();

        System.out.println("Logic2 Soal 2: ");
        Soal2_2 soal2_2 = new Soal2_2(9);
        soal2_2.cetakArray();

        System.out.println("Logic2 Soal 5: ");
        Logic.Logic2.Soal5 soal51 = new Logic.Logic2.Soal5(9);
        soal51.cetakArray();



    }
}
