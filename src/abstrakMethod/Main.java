package abstrakMethod;

public class Main {
    public static void main(String[] args) {

        System.out.println("Cat: ");
        Cat cat = new Cat(" Kittun");
        cat.run();
        cat.life();
        cat.eat();
        cat.characteristic();
        cat.spesies();

        System.out.println("Fish: ");

        Fish fish = new Fish(" Galah");
        fish.run(); fish.eat(); fish.life(); fish.characteristic(); fish.spesies();

        System.out.println("Lion: ");
        Lion lion = new Lion(" Simba");
        lion.run(); lion.eat(); lion.life(); lion.characteristic(); lion.spesies();
        System.out.println("Chicken: ");
        Chicken chicken = new Chicken(" Fried Chicken");
        chicken.eat(); chicken.spesies(); chicken.characteristic(); chicken.life(); chicken.run();

        System.out.println("Fish: ");

    }
}
