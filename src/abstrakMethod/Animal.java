package abstrakMethod;

public abstract class Animal {
    String name;
    abstract void run();
    abstract void eat();
    abstract void life();
    abstract void spesies();

    abstract void characteristic();

}
