package abstrakMethod;

public class Fish extends Animal{

    public Fish(String name){
        this.name = name;
    }
    @Override
    void run() {
        System.out.println("The Fish Name is" +name+ ", and Fish can be Swimming");
    }

    @Override
    void eat() {
        System.out.println("The cat eat shrimp");
    }
    @Override
    void life() {
        System.out.println("The Fish" +name+ ", and Fish can life with River or Sea");
    }

    @Override
    void spesies() {
        System.out.println("The Cat" +name+ ", spesies of Omnivora");
    }
    @Override
    void characteristic() {
        System.out.println("The Charcateristic fish is having gill ");

    }
}
