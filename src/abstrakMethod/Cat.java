package abstrakMethod;

public class Cat extends Animal{

    public Cat(String name){
        this.name = name;
    }
    @Override
    void run() {
        System.out.println("The Cat" +name+ ", can be Run");
    }

    @Override
    void eat() {
        System.out.println("The cat eat meat");

    }

    @Override
    void life() {
        System.out.println("The Cat" +name+ ", can life with Human");

    }

    @Override
    void spesies() {
        System.out.println("The Cat" +name+ ", spesies of Omnivora");

    }

    @Override
    void characteristic() {
        System.out.println("The Charcateristic cat is having moustache ");

    }
}
