package SuperKeyword;

public class MainSuper {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        System.out.println("Get Name: "+rectangle.getName());
        System.out.println("Get Corner: " + rectangle.getCorner());
        System.out.println("Get Parent Corner: " + rectangle.getParentCorner());
    }
}
