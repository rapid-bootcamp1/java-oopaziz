package Logic.LogicInterface.Logic1Impl;

import Logic.BaseLogic;
import Logic.LogicInterface.LogicInterface;

public class Soal1Impl implements LogicInterface {

    private final BaseLogic baseLogic;

    public Soal1Impl(BaseLogic baseLogic) {
        this.baseLogic = baseLogic;
    }

    public void isiArray(){
        int angka = 1;
        for (int i = 0; i < this.baseLogic.n; i++) {
            this.baseLogic.array[0][i]= Integer.valueOf(String.valueOf(angka));
            angka = angka+1;
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.baseLogic.printSingle();


    }
}
