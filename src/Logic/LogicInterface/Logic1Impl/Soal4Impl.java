package Logic.LogicInterface.Logic1Impl;

import Logic.BaseLogic;
import Logic.LogicInterface.LogicInterface;

public class Soal4Impl implements LogicInterface {

    private final BaseLogic baseLogic;

    public Soal4Impl(BaseLogic baseLogic) {
        this.baseLogic = baseLogic;
    }
    public void isiArray(){
        for (int i = 0; i < this.baseLogic.n; i++){
            if(i ==0 || i ==1){
                // deret ke i => 0, 1
                this.baseLogic.array[0][i] = 1;
            }else{
                this.baseLogic.array[0][i] = this.baseLogic.array[0][i-1] + this.baseLogic.array[0][i-2];
            }
        }
    }

    @Override
    public void cetakArray() {
        isiArray();
        this.baseLogic.printSingle();
    }
}
