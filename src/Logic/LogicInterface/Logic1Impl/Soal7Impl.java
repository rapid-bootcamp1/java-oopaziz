package Logic.LogicInterface.Logic1Impl;

import Logic.BaseLogic;
import Logic.LogicInterface.LogicInterface;

public class Soal7Impl implements LogicInterface {

    private final BaseLogic baseLogic;

    public Soal7Impl(BaseLogic baseLogic) {
        this.baseLogic = baseLogic;
    }
    public void isiArray(){
        char huruf = 'A';
        for (int i = 0; i < this.baseLogic.n; i++){
            String datahuruf = String.valueOf(this.baseLogic.array[0][i]);
            datahuruf = String.valueOf(huruf);
            huruf++;
            System.out.print(datahuruf +"\t");
        }
    }

    @Override
    public void cetakArray() {
        isiArray();
        this.baseLogic.printSingle();
    }
}
