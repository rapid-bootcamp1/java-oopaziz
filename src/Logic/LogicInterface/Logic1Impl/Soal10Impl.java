package Logic.LogicInterface.Logic1Impl;

import Logic.BaseLogic;
import Logic.LogicInterface.LogicInterface;

public class Soal10Impl implements LogicInterface {

    private final BaseLogic baseLogic;

    public Soal10Impl(BaseLogic baseLogic) {
        this.baseLogic = baseLogic;
    }
    public void isiArray(){
        for (int i = 0; i < this.baseLogic.n; i++){
            this.baseLogic.array[0][i] = (int) Math.pow(i,3);

        }
    }

    @Override
    public void cetakArray() {
        isiArray();
        this.baseLogic.printSingle();
    }
}
