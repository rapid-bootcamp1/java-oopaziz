package Logic.LogicInterface.Logic1Impl;

import Logic.BaseLogic;
import Logic.LogicInterface.LogicInterface;

public class Soal3Impl implements LogicInterface {
    private final BaseLogic baseLogic;

    public Soal3Impl(BaseLogic baseLogic) {
        this.baseLogic = baseLogic;
    }

    public void isiArray(){
        for (int i = 0; i < this.baseLogic.n; i++) {
            System.out.print(i*2+ "\t");
        }
    }

    @Override
    public void cetakArray() {
        isiArray();
        this.baseLogic.printSingle();

    }
}
