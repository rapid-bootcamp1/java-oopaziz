package Logic.LogicInterface.Logic1Impl;

import Logic.BaseLogic;
import Logic.LogicInterface.LogicInterface;

public class Soal8Impl implements LogicInterface {

    private final BaseLogic baseLogic;

    public Soal8Impl(BaseLogic baseLogic) {
        this.baseLogic = baseLogic;
    }
    public void isiArray(){
        char huruf = 'A';
        int angka = 2;


        for (int i = 0; i < this.baseLogic.n; i++) {
            String datahuruf = String.valueOf(this.baseLogic.array[0][i]);
            if (i % 2 == 0) {
                datahuruf = String.valueOf(huruf);
            } else {
                datahuruf = String.valueOf(angka);
                angka += 2;

            }
            huruf++;
            System.out.print(datahuruf +"\t");
        }
    }

    @Override
    public void cetakArray() {
        isiArray();
        this.baseLogic.printSingle();
    }
}
