package Logic.LogicInterface.Logic1Impl;

import Logic.BaseLogic;
import Logic.LogicInterface.LogicInterface;

public class Soal6Impl implements LogicInterface {

    private final BaseLogic baseLogic;

    public Soal6Impl(BaseLogic baseLogic) {
        this.baseLogic = baseLogic;
    }
    public void isiArray(){
        for (int i = 0; i < this.baseLogic.n; i++){
            boolean isPrima = true;
            for (int j = 2; j < i; j++){
                if(i%j ==0){
                    isPrima = false;
                    break;
                }
            }
            if (isPrima == true){
                System.out.print(i+ "\t");
            }
        }
    }

    @Override
    public void cetakArray() {
        isiArray();
        this.baseLogic.printSingle();
    }
}
