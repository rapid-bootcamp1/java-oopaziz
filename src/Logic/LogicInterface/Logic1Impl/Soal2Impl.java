package Logic.LogicInterface.Logic1Impl;

import Logic.BaseLogic;
import Logic.LogicInterface.LogicInterface;

public class Soal2Impl implements LogicInterface {

    private final BaseLogic baseLogic;

    public Soal2Impl(BaseLogic baseLogic) {
        this.baseLogic = baseLogic;
    }

    public void isiArray(){
        int a = 0;
        int b = 0;
        for (int i = 0; i < this.baseLogic.n; i++) {
            if(i%2 == 0){
                if(i == 0){
                    a = 1;
                    System.out.print(a+"\t");
                }else{
                    System.out.print(a+"\t");
                }
            }else{
                b = a*3;
                System.out.print(b+"\t");
                a = b-i;
            }
        }

    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.baseLogic.printSingle();

    }
}
