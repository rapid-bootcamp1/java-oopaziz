package Logic.LogicInterface.Logic1Impl;

import Logic.BaseLogic;
import Logic.LogicInterface.LogicInterface;

public class Soal9Impl implements LogicInterface {

    private final BaseLogic baseLogic;

    public Soal9Impl(BaseLogic baseLogic) {
        this.baseLogic = baseLogic;
    }
    public void isiArray(){
        for (int i = 0; i < this.baseLogic.n; i++){
            this.baseLogic.array[0][i] = (int) Math.pow(3,i);

        }
    }

    @Override
    public void cetakArray() {
        isiArray();
        this.baseLogic.printSingle();
    }
}
