package Logic.LogicInterface.Logic2Impl;

import Logic.BaseLogic;
import Logic.LogicInterface.LogicInterface;

public class Soal02Impl implements LogicInterface {

    private final BaseLogic baseLogic;

    public Soal02Impl(BaseLogic baseLogic) {
        this.baseLogic = baseLogic;
    }
    public void isiArray() {
        for (int i = 0; i < this.baseLogic.n; i++) {
            int angka = 1;
            for (int j = 0; j < this.baseLogic.n; j++) {
                if(j==0 || i==0  || i == this.baseLogic.n-1 || j== this.baseLogic.n-1 ){
                    this.baseLogic.array2[i][j] = String.valueOf(angka);
                }
                angka+=2;
            }
        }

    }

    @Override
    public void cetakArray() {
        isiArray();
        this.baseLogic.print2();
    }
}
