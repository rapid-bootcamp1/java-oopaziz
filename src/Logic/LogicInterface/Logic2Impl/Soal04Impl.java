package Logic.LogicInterface.Logic2Impl;

import Logic.BaseLogic;
import Logic.LogicInterface.LogicInterface;

public class Soal04Impl implements LogicInterface {

    private final BaseLogic baseLogic;

    public Soal04Impl(BaseLogic baseLogic) {
        this.baseLogic = baseLogic;
    }
    public void isiArray() {

        for(int i = 0; i< baseLogic.n; i++){
            for(int j = 0; j< baseLogic.n; j++){
                if(i == 0 && j<=1 ) {
                    this.baseLogic.array2[i][j]= String.valueOf(1);
                } else if(i==0) {
                    this.baseLogic.array2[i][j] = String.valueOf(Integer.parseInt(this.baseLogic.array2[i][j-1]) +
                            Integer.parseInt(this.baseLogic.array2[i][j-2]));
                }
                else if
                (j == 0 || j== baseLogic.n/2 || j== baseLogic.n-1 || i == baseLogic.n/2 || i == baseLogic.n-1) {
                    this.baseLogic.array2[i][j] =this.baseLogic.array2[0][j];
                }


            }
        }


    }

    @Override
    public void cetakArray() {
        isiArray();
        this.baseLogic.print2();
    }
}
