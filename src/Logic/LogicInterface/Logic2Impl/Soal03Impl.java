package Logic.LogicInterface.Logic2Impl;

import Logic.BaseLogic;
import Logic.LogicInterface.LogicInterface;

public class Soal03Impl implements LogicInterface {

    private final BaseLogic baseLogic;

    public Soal03Impl(BaseLogic baseLogic) {
        this.baseLogic = baseLogic;
    }
    public void isiArray() {
//        for(int i = 0; i< baseLogic.n; i++){
//            for(int j = 0; j< baseLogic.n; j++){
//                if(j==0) this.baseLogic.array2[i][j]= String.valueOf(0);
//                else if (i == 0)this.baseLogic.array2[i][j] = String.valueOf(Integer.parseInt(this.baseLogic.array2[i][j-1]) + 1);
//                else if(i == j || i == baseLogic.n-j-1 || i == baseLogic.n-1 || j == baseLogic.n-1)this.baseLogic.array2[i][j] = this.baseLogic.array2[i][j];
//                else this.baseLogic.array2[i][j] = ".";
//            }
//        }
        for (int i = 0; i < this.baseLogic.n; i++) {
            int angka = 1;
            for (int j = 0; j < this.baseLogic.n; j++) {
                if(j==0 || i==0  || i == this.baseLogic.n-1 || j== this.baseLogic.n-1 ||
                        i==j || i+j == this.baseLogic.n-1){
                    this.baseLogic.array2[i][j] = String.valueOf(angka);
                }
                angka+=2;
            }
        }


    }

    @Override
    public void cetakArray() {
        isiArray();
        this.baseLogic.print2();
    }
}
