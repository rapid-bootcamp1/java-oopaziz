package Logic.LogicInterface.Logic2Impl;

import Logic.BaseLogic;
import Logic.LogicInterface.LogicInterface;

public class Soal01Impl implements LogicInterface {

    private final BaseLogic baseLogic;

    public Soal01Impl(BaseLogic baseLogic) {
        this.baseLogic = baseLogic;
    }
    public void isiArray() {
        for (int i = 0; i < this.baseLogic.n; i++) {
            int angka = 1;
            for (int j = 0; j < this.baseLogic.n; j++) {
                if (i == j || i + j == this.baseLogic.n - 1) {
                    this.baseLogic.array2[i][j] = String.valueOf(angka);
                }
                angka += 1;
            }
        }
    }

    @Override
    public void cetakArray() {
        isiArray();
        this.baseLogic.print2();
    }
}
