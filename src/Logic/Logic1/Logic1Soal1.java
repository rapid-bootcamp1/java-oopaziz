package Logic.Logic1;

import Logic.BaseLogic;

public class Logic1Soal1 extends BaseLogic {

    public Logic1Soal1(int n) {
        super(n);
    }

    public void isiArray(){
        int angka = 1;
        for (int i = 0; i < this.n; i++) {
            this.array[0][i]= angka;
            angka = angka+1;
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }

}
