package Logic.Logic1;

import Logic.BaseLogic;

public class Soal3 extends BaseLogic {
    public Soal3(int n) {
        super(n);
    }
    public void isiArray(){
        for (int i = 0; i < this.n; i++) {
            System.out.print(i*2+ "\t");
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
