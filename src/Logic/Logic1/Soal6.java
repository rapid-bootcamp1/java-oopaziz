package Logic.Logic1;

import Logic.BaseLogic;

public class Soal6 extends BaseLogic {
    public Soal6(int n) {
        super(n);
    }

    public void isiArray(){

        for (int i = 0; i < this.n; i++){
            boolean isPrima = true;
            for (int j = 2; j < i; j++){
                if(i%j ==0){
                    isPrima = false;
                    break;
                }
            }
            if (isPrima == true){
                System.out.print(i+ "\t");
            }
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
