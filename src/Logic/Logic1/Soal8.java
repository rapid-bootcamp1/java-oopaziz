package Logic.Logic1;

import Logic.BaseLogic;

public class Soal8 extends BaseLogic {
    public Soal8(int n) {
        super(n);
    }

    public void isiArray(){
        char huruf = 'A';
        int angka = 2;


        for (int i = 0; i < this.n; i++){
            String datahuruf = String.valueOf(this.array[0][i]);
            if(i % 2 == 0){
                datahuruf = String.valueOf(huruf);
            }else{
                datahuruf = String.valueOf(angka);
                angka+=2;

            }
            huruf++;
            System.out.print(datahuruf +"\t");
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }

}
