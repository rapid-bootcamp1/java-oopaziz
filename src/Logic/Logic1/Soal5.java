package Logic.Logic1;

import Logic.BaseLogic;

public class Soal5 extends BaseLogic {
    public Soal5(int n) {
        super(n);
    }
    public void isiArray(){

        for (int i = 0; i < this.n; i++){
            if(i ==0 || i ==1 || i==2){
                // deret ke i => 0, 1
                this.array[0][i] = 1;
            }else{
                this.array[0][i] = this.array[0][i-1] + this.array[0][i-2] + this.array[0][i-3];
            }
        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
