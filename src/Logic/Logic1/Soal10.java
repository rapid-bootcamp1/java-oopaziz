package Logic.Logic1;

import Logic.BaseLogic;

public class Soal10 extends BaseLogic {
    public Soal10(int n) {
        super(n);
    }
    public void isiArray(){

        for (int i = 0; i < this.n; i++){
            this.array[0][i] = (int) Math.pow(i,3);

        }
    }
    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
