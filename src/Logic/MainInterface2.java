package Logic;

import Logic.Logic2.Soal1;
import Logic.LogicInterface.Logic2Impl.*;
import Logic.LogicInterface.LogicInterface;

public class MainInterface2 {
    public static void main(String[] args) {
        System.out.println("Logic 1 Soal 1 Interface: ");
        LogicInterface logic2Soal1 = new Soal01Impl(new BaseLogic(9));
        logic2Soal1.cetakArray();

        System.out.println("Logic 2 Soal 1 Interface: ");
        LogicInterface logic2Soal2 = new Soal02Impl(new BaseLogic(9));
        logic2Soal2.cetakArray();

        System.out.println("Logic 3 Soal 1 Interface: ");
        LogicInterface logic2Soal3 = new Soal03Impl(new BaseLogic(9));
        logic2Soal3.cetakArray();

        System.out.println("Logic 4 Soal 1 Interface: ");
        LogicInterface logic2Soal4 = new Soal04Impl(new BaseLogic(9));
        logic2Soal4.cetakArray();

        System.out.println("Logic 5 Soal 1 Interface: ");
        LogicInterface logic2Soal5 = new Soal05Impl(new BaseLogic(9));
        logic2Soal5.cetakArray();
    }
}
