package Logic;

import Logic.LogicInterface.Logic1Impl.*;
import Logic.LogicInterface.LogicInterface;

public class MainInterface {
    public static void main(String[] args) {
        LogicInterface logic1Soal1 = new Soal1Impl(new BaseLogic(9));
        System.out.println("Logic 1 Soal 1 Interface: ");
        logic1Soal1.cetakArray();

        System.out.println("Logic 1 Soal 2 Interface: ");
        LogicInterface logic1Soal2 = new Soal2Impl(new BaseLogic(9));
        logic1Soal2.cetakArray();

        System.out.println("Logic 1 Soal 3 Interface: ");
        LogicInterface logic1Soal3 = new Soal3Impl(new BaseLogic(9));
        logic1Soal3.cetakArray();

        System.out.println("Logic 1 Soal 4 Interface: ");
        LogicInterface logic1Soal4 = new Soal4Impl(new BaseLogic(9));
        logic1Soal4.cetakArray();

        System.out.println("Logic 1 Soal 5 Interface: ");
        LogicInterface logic1Soal5 = new Soal5Impl(new BaseLogic(9));
        logic1Soal5.cetakArray();

        System.out.println("Logic 1 Soal 6 Interface: ");
        LogicInterface logic1Soal6 = new Soal6Impl(new BaseLogic(9));
        logic1Soal6.cetakArray();

        System.out.println("Logic 1 Soal 7 Interface: ");
        LogicInterface logic1Soal7 = new Soal7Impl(new BaseLogic(9));
        logic1Soal7.cetakArray();

        System.out.println("Logic 1 Soal 8 Interface: ");
        LogicInterface logic1Soal8 = new Soal8Impl(new BaseLogic(9));
        logic1Soal8.cetakArray();

        System.out.println("Logic 1 Soal 9 Interface: ");
        LogicInterface logic1Soal9 = new Soal9Impl(new BaseLogic(9));
        logic1Soal9.cetakArray();

        System.out.println("Logic 1 Soal 10 Interface: ");
        LogicInterface logic1Soal10 = new Soal10Impl(new BaseLogic(9));
        logic1Soal10.cetakArray();
    }
}
