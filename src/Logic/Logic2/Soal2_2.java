package Logic.Logic2;

import Logic.BaseLogic;

public class Soal2_2 extends BaseLogic {

    public Soal2_2(int n) {
        super(n);
    }

    public void isiArray(){
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(j==0){
                    this.array2[i][j] = String.valueOf(1);
                }else if(i==0 && j>=1){
                    this.array2[i][j] = String.valueOf(Integer.parseInt(this.array2[i][j-1]+2));
                } else if (j == n-1 || i == n-1) {
                    this.array2[i][j] = this.array2[0][j];
                }else{
                    this.array2[i][j] = ".";
                }
            }
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.print();
    }
}
