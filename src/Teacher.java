public class Teacher extends  Person{
    private Integer IdTeacher;
    private String Mapel;
    private String Sekolah;

    public Teacher(Integer id, String name, String gender, String address, int age, Integer idTeacher, String mapel, String sekolah) {
        super(id, name, gender, address, age);
        IdTeacher = idTeacher;
        Mapel = mapel;
        Sekolah = sekolah;
    }
}
